/*
 * StorageHandler.cpp
 *
 *  Created on: Mar 4, 2014
 \*      Author: Jonas Kunze (kunze.jonas@gmail.com)
 */

#include "StorageHandler.h"

#include <boost/algorithm/string.hpp>
#include <asm-generic/errno-base.h>
#include <eventBuilding/Event.h>
#include <eventBuilding/SourceIDManager.h>
#include <sstream>

//#include <structs/Event.h>
//#include <structs/Versions.h>
#include <zmq.h>
#include <zmq.hpp>
#include <cstdbool>
#include <cstdint>
#include <cstring>
#include <iostream>
#include <string>
#include <thread>
#include <utility> //used for std::pair
#include <socket/ZMQHandler.h>
#include <glog/logging.h>

#include "../options/MyOptions.h"
#include <storage/SmartEventSerializer.h>

namespace na62 {

std::vector<zmq::socket_t*> StorageHandler::mergerSockets_;
std::vector<zmq::socket_t*> StorageHandler::monitorSockets_;

tbb::concurrent_queue< std::pair< const EVENT_HDR*, bool> > StorageHandler::DataQueue_; //queue of pairs, the bool is a flag to enable monitor sending of the event

std::recursive_mutex StorageHandler::sendMutex_;


void StorageHandler::setMonitors(std::vector<std::string> monitorList) {
	std::lock_guard<std::recursive_mutex> my_lock(sendMutex_);
	for (auto socket : monitorSockets_) {
		ZMQHandler::DestroySocket(socket);
	}
	monitorSockets_.clear();

	for (auto host : monitorList) {
		try {
			std::stringstream address;
			address << "tcp://"<< host << ":" << Options::GetInt(OPTION_MERGER_PORT);
			zmq::socket_t* socket = ZMQHandler::GenerateSocket("MonitorHandler", ZMQ_PUSH);
			socket->connect(address.str().c_str());
			monitorSockets_.push_back(socket);
		} catch (const zmq::error_t& ex) {
			LOG_ERROR("Failed to initialize ZMQ for monitor " << host << " because: " << ex.what());
			throw ex;
		}
	}
}

void StorageHandler::setMergers(std::vector<std::string> mergerList) {
	std::lock_guard<std::recursive_mutex> my_lock(sendMutex_);
	for (auto socket : mergerSockets_) {
		ZMQHandler::DestroySocket(socket);
	}
	mergerSockets_.clear();

	for (auto host : mergerList) {
		try {
			std::stringstream address;
			address << "tcp://"<< host << ":" << Options::GetInt(OPTION_MERGER_PORT);
			zmq::socket_t* socket = ZMQHandler::GenerateSocket("MergerHandler", ZMQ_PUSH);
			socket->connect(address.str().c_str());
			mergerSockets_.push_back(socket);
		} catch (const zmq::error_t& ex) {
			LOG_ERROR("Failed to initialize ZMQ for merger " << host << " because: " << ex.what());
			throw ex;
		}
	}
}

void StorageHandler::initializeMonitors() {
	setMonitors(Options::GetStringList(OPTION_MONITOR_HOST_NAMES));
}
void StorageHandler::initializeMergers() {
	setMergers(Options::GetStringList(OPTION_MERGER_HOST_NAMES));
}
void StorageHandler::initialize() {
	StorageHandler::initializeMergers();
	StorageHandler::initializeMonitors();
}


void StorageHandler::onShutDown() {
	std::lock_guard<std::recursive_mutex> my_lock(sendMutex_);
	for (auto socket : mergerSockets_) {
		ZMQHandler::DestroySocket(socket);
	}
	for (auto socket : monitorSockets_) {
		ZMQHandler::DestroySocket(socket);
	}
	mergerSockets_.clear();
	monitorSockets_.clear();
}

int StorageHandler::SendEvent(const Event* event) {
	/*
	 * TODO: Use multimessage instead of creating a separate buffer and copying the MEP data into it
	 */
	const EVENT_HDR* data = SmartEventSerializer::SerializeEvent(event);
	int dataLength = data->length * 4;

	bool monitor = false;
	if(Options::GetBool(OPTION_MONITOR_PASS_ALL)) {
		monitor = true;
	} else {
		uint tfm = Options::GetInt(OPTION_MONITOR_L0TP_FLAGS_BITMASK); //fetching of the trigger word
		uint twm = Options::GetInt(OPTION_MONITOR_L0TP_TYPE_BITMASK);
		monitor = (event->getTriggerFlags() & tfm) || (event->getL0TriggerTypeWord() & twm);
	}
	StorageHandler::DataQueue_.push(std::make_pair(data, monitor));
	return dataLength;
}

void StorageHandler::thread() {
	while (running_) {
		std::pair< const EVENT_HDR*, bool > pkg; //pair element, first is pointer to event header, second is monitor flag
		if (StorageHandler::DataQueue_.try_pop(pkg)) {
			try {
				zmq::message_t zmqMessageMerger((void*) pkg.first, pkg.first->length * 4,
						(zmq::free_fn*) ZMQHandler::freeZmqMessage);
				
				if(Options::GetStringList(OPTION_MONITOR_HOST_NAMES).size()) {
					zmq::message_t zmqMessageMonitor;
					zmqMessageMonitor.copy(&zmqMessageMerger);

					if (pkg.second) {
						std::lock_guard<std::recursive_mutex> my_lock(sendMutex_);
						try {
							monitorSockets_[pkg.first->burstID % monitorSockets_.size()]->send(zmqMessageMonitor);
						} catch (const zmq::error_t& ex) {
							if (ex.num() != EINTR) { // try again if EINTR (signal caught)
								LOG_ERROR(ex.what());
								try {
									initializeMonitors(); //try to re-initialize mergers
								} catch (const zmq::error_t& exx) {
									LOG_ERROR(exx.what());
								}
							}
						}
					}
				}

				while (ZMQHandler::IsRunning()) {
					std::lock_guard<std::recursive_mutex> my_lock(sendMutex_);
					try {
						mergerSockets_[pkg.first->burstID % mergerSockets_.size()]->send(zmqMessageMerger);
						break; //fallback on normal implementation

					}
					catch (const zmq::error_t& ex) {
						if (ex.num() != EINTR)
						{ // try again if EINTR (signal caught)
							LOG_ERROR(ex.what());
							try
							{
								initializeMergers(); //try to re-initialize mergers
							}
							catch (const zmq::error_t& exx)
							{
								LOG_ERROR(exx.what());
							}
						}
					}
				}
			}
			catch (const zmq::error_t& e) {
				LOG_ERROR("Failed to create ZMQ messages, because: " << e.what());
			}
		}
		else {
			boost::this_thread::sleep(boost::posix_time::microsec(50));
		}
	}
}
void StorageHandler::onInterruption() {
		running_ = false;
		}
} /* namespace na62 */
